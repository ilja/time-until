<!--
SPDX-FileCopyrightText: 2023 ilja@ilja.space

SPDX-License-Identifier: AGPL-3.0-only
-->

A simple webpage to show a countdown. It shows a smily and the closer it gets to the target time, the bigger the smile gets.

The parameters are provided via the url with query parameters.

<https://example.org?description=Time%20to%20reaching%20paradise&start_time=2006-08-01%2007:15:00&target_time=2049-06-30%2016:00:00>

## Scope

* A landing page where parameters can be provided (and somehow link to a page with the correct parameters)
* Maybe an extra bar instead of only smily
* A functional version of the tool should always remain in a single html file

## License

AGPLv3.0
